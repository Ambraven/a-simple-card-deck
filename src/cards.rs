use std::path::Path;
use rand::{Rng, thread_rng};
use bevy::prelude::*;
use enum_iterator::IntoEnumIterator;
use serde::{Serialize,Deserialize};

#[derive(Debug, Clone, Copy, PartialEq, IntoEnumIterator, Serialize, Deserialize)]
pub enum CardColor {
    Coeur,
    Carreau,
    Pique,
    Trefle
}

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, IntoEnumIterator, Serialize, Deserialize)]
#[repr(u8)]
pub enum CardValue {
    As,
    Deux,
    Trois,
    Quatre,
    Cinq,
    Six,
    Sept,
    Huit,
    Neuf,
    Dix,
    Valet,
    Dame,
    Roi
}

#[derive(Component, Serialize, Deserialize)]
pub struct Card {
    pub color : CardColor,
    pub value : CardValue
}

#[derive(Component, Serialize, Deserialize)]
pub struct Deck {
    pub name : String,
    pub cards : Vec<Entity>
}

pub enum DeckCommands {
    ShuffleDeck,
    SuffleHand,
    DiscardHand,
    MakeFollowingLeadDeck,
    Save,
    Load
}

pub struct CardsPlugin;

impl Plugin for CardsPlugin {
    fn build(&self, app: &mut App) {
        app
            .add_startup_system(add_cards)
            .add_system(deck_system)
        ;
    }
}

pub struct Game {
    pub deck : Deck,
    pub hand : Vec<Entity>,
    pub discard : Vec<Entity>
}

fn add_cards(mut commands : Commands) {
    let mut game = Game{
        deck : Deck{name : "Default Deck".to_string(), cards: Vec::new()},
        hand : Vec::new(),
        discard : Vec::new()
    };
    let deck_commands : Vec<DeckCommands> = Vec::new();
    for color in CardColor::into_enum_iter() {
        for value in CardValue::into_enum_iter() {
            let id = commands.spawn()
                .insert(Card {color, value})
                .id();
            game.deck.cards.push(id);
        }
    }
    commands
        .insert_resource(game);
    commands
        .insert_resource(deck_commands);
}

fn deck_system(
    mut _commands: Commands,
    mut deck_commands: ResMut<Vec<DeckCommands>>,
    mut game: ResMut<Game>,
    query : Query<(Entity, &Card)>,
) {
    for command in deck_commands.iter() {
        match command {
            DeckCommands::ShuffleDeck => shuffle(&mut game.deck.cards),
            DeckCommands::SuffleHand => shuffle(&mut game.hand),
            DeckCommands::DiscardHand => {
                while let Some(card) = game.hand.pop(){
                    game.discard.push(card);
                }
            }
            DeckCommands::MakeFollowingLeadDeck => {
                let mut full_deck : Vec<(Entity, &Card)> = Vec::new();
                for (e,c) in query.iter() {
                    full_deck.push((e,c));
                }
                game.deck.cards = make_following_lead_deck(full_deck)
            }
            DeckCommands::Save => {
                let mut deck_to_save : Vec<&Card> = Vec::new();
                for card_entity in game.deck.cards.iter() {
                    if let Some((_e, card)) = query.iter().find(|(entity, _card)| entity == card_entity) {
                        deck_to_save.push(card);
                    }
                }
                save_deck(&mut deck_to_save);
            }
            DeckCommands::Load => {
                game.deck.cards.clear();
                let deck_to_load = load_deck();
                for new_card in deck_to_load.iter() {
                    if let Some((entity, _card)) = query.iter().find(|(_entity, card)| card.color == new_card.color && card.value == new_card.value) {
                        game.deck.cards.push(entity);
                    }
                }
            }
        };
    }
    deck_commands.clear();
}

fn shuffle(card_set: &mut Vec<Entity>) {
    let mut rng = thread_rng();
    let len = card_set.len();
    for i in (1..len).rev() {
        let j = rng.gen_range(0..i);
        card_set.swap(i, j);
    }
}

fn make_following_lead_deck(cards: Vec<(Entity, &Card)>) -> Vec<Entity> {
    println!("coucou");
    let mut rng = thread_rng();
    let mut new_deck : Vec<Entity> = Vec::new();

    let mut heart_deck = cards.iter().filter(|(_entity, card)| card.color == CardColor::Coeur).collect::<Vec<_>>();
    let mut diamon_deck = cards.iter().filter(|(_entity, card)| card.color == CardColor::Carreau).collect::<Vec<_>>();
    let mut clover_deck = cards.iter().filter(|(_entity, card)| card.color == CardColor::Trefle).collect::<Vec<_>>();
    let mut spade_deck = cards.iter().filter(|(_entity, card)| card.color == CardColor::Pique && card.value < CardValue::Valet).collect::<Vec<_>>();

    new_deck.push(spade_deck.drain_filter(|(_e, card)| card.value == CardValue::As).collect::<Vec<_>>().first().unwrap().0); // the ace of spade is the end of the game

    for _i in 0..6 {
        let card_id = rng.gen_range(0..spade_deck.len());
        new_deck.push(spade_deck.remove(card_id).0);
    }

    for _i in 0..6 {
        let card_id = rng.gen_range(0..clover_deck.len());
        new_deck.push(clover_deck.remove(card_id).0);
    }

    for _i in 0..6 {
        let card_id = rng.gen_range(0..diamon_deck.len());
        new_deck.push(diamon_deck.remove(card_id).0);
    }

    for _i in 0..6 {
        let card_id = rng.gen_range(0..heart_deck.len());
        new_deck.push(heart_deck.remove(card_id).0);
    }

    new_deck
}

fn save_deck(deck_to_save: &mut Vec<&Card>) {
    let file = Path::new("./saved_deck.json");
    let raw = serde_json::to_vec(deck_to_save).expect("unable to serialize");
    std::fs::write(file, raw).expect("unable to write file");
}

fn load_deck() -> Vec<Card> {
    let raw = std::fs::read("./saved_deck.json").expect("unable to read file");
    serde_json::from_slice(&raw).expect("unable to parse file")
}
