#![feature(drain_filter)]

mod cards;

use bevy::prelude::*;
use bevy_egui::{egui, EguiContext, EguiPlugin};
use crate::cards::{Card, CardsPlugin, Game, DeckCommands};

fn print_hello(mut egui_context: ResMut<EguiContext>,
               mut deck_commands: ResMut<Vec<DeckCommands>>,
               query : Query<(Entity, &Card)>,
               mut game : ResMut<Game>,) {
    egui::Window::new("Hello").show(egui_context.ctx_mut(), |ui| {
        ui.label("world");
        if ui.button("Click to Shuffle").clicked() {
            println!("shuffle!");
            deck_commands.push(DeckCommands::ShuffleDeck);
        }
        if ui.button("Generate a Following the Leads Deck").clicked() {
            println!("new story!");
            deck_commands.push(DeckCommands::MakeFollowingLeadDeck);
        }
        if ui.button("Print all cards in deck").clicked() {
            for card_id in game.deck.cards.iter() {
                if let Ok((_entity, card)) = query.get(*card_id) {
                    println!("{:?} de {:?}", card.value, card.color);
                }
            }
        }
        if ui.button("Count cards").clicked() {
            println!("There are {} cards in the deck", game.deck.cards.len());
        }
        if ui.button("Draw a card").clicked() {
            if let Some(card) = game.deck.cards.pop() {
                game.hand.push(card);
                let (_e, card) = query.get(card).unwrap();
                println!("{:?} de {:?} est tiré.", card.value, card.color);
            }
        }
        if ui.button("Save").clicked() {
            println!("Save!");
            deck_commands.push(DeckCommands::Save);
        }
        if ui.button("Load").clicked() {
            println!("Load!");
            deck_commands.push(DeckCommands::Load);
        }
    });
}

fn main()
{
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugin(EguiPlugin)
        .add_plugin(CardsPlugin)
        .add_system(print_hello)
        .run();
}